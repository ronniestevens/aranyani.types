# -*- coding: utf-8 -*-
"""Setup/installation tests for this package."""

from aranyani.types.testing import IntegrationTestCase
from plone import api


class TestInstall(IntegrationTestCase):
    """Test installation of aranyani.types into Plone."""

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if aranyani.types is installed with portal_quickinstaller."""
        self.assertTrue(self.installer.isProductInstalled('aranyani.types'))

    def test_uninstall(self):
        """Test if aranyani.types is cleanly uninstalled."""
        self.installer.uninstallProducts(['aranyani.types'])
        self.assertFalse(self.installer.isProductInstalled('aranyani.types'))

    # browserlayer.xml
    def test_browserlayer(self):
        """Test that IAranyaniTypesLayer is registered."""
        from aranyani.types.interfaces import IAranyaniTypesLayer
        from plone.browserlayer import utils
        self.assertIn(IAranyaniTypesLayer, utils.registered_layers())
