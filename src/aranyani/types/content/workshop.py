from plone.app.textfield import RichText
from plone.namedfile import field as namedfile
from plone.supermodel.directives import fieldset
from plone.supermodel import model
from zope import schema

from aranyani.types import _


class IWorkshop(model.Schema):
    """Dexterity-Schema for Workshop
    """

    text = RichText(
        title=_(u"MyText"),
        required=False
        )

    date = schema.Date(
        title=_(u"Datum"),
        required=False
        )

    fieldset('Files', fields=['verslag', 'presentatie'])
    verslag = namedfile.NamedBlobFile(
        title=_(u"Verslag"),
        required=False
        )

    presentatie = namedfile.NamedBlobFile(
        title=_(u"presentatie"),
        required=False
        )
