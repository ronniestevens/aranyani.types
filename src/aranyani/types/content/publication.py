from plone.namedfile import field as namedfile
from plone.supermodel.directives import fieldset
from plone.supermodel import model
from zope import schema

from aranyani.types import _


class IPublication(model.Schema):
    """Dexterity-Schema for Publication
    """

    surname = schema.TextLine(
        title=_(u"Achternaam"),
        required=True)

    year = schema.TextLine(
        title=_(u"Jaar"),
        description=_(u"Vul een heel getal in"),
        required=False,)

    source = schema.TextLine(
        title=_(u"Bron"),
        required=False)

    publisher = schema.TextLine(
        title=_(u"uitgever"),
        required=False)

    url = schema.URI(
        title=_(u"link"),
        description=_(u"Vul een valide url"),
        required=False)

    fieldset('Files', fields=['pdf'])
    pdf = namedfile.NamedBlobFile(
        title=_(u"PDF"),
        required=False)
