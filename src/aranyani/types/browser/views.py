# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from plone.dexterity.browser.view import DefaultView
from plone import api

import logging
logger = logging.getLogger(__name__)


class PublicationView(DefaultView):
    """Browserview for publication content type
    """


class PublicationListView(BrowserView):

    def publicationlist(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        result = []
        for pub in catalog(
            {'portal_type': 'publication',
             'sort_on': 'getObjPositionInParent',
             'sort_order': 'ascending',
             'path': dict(query=path, depth=10)}
        ):
            obj = pub.getObject()
            result.append(
                dict(title=pub.Title,
                     url=pub.getURL(),
                     description=pub.Description,
                     year=obj.year,
                     date=pub.EffectiveDate)
            )
        return result


class EventListView(BrowserView):

    def eventlist(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        brains = catalog (
            {
                'portal_type': 'Event',
                'path': dict(query=path, depth=10),
                'sort_on': 'start',
                'sort_order': 'ascending',
            }
        )
        result = [x for x in brains]

        return result